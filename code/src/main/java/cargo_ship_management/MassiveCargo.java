package cargo_ship_management;

public class MassiveCargo extends Cargo{
    
    /**
     * Constructor for MassiveCargo
     * @param goodsType
     * @param weight
     * @param volume
     * @param destination
     */
    public MassiveCargo(String goodsType, double weight, double volume, String destination) {
        super(goodsType, weight, volume, destination);
    }

    /**
     * Copy constructor
     * @param cargo
     */
    public MassiveCargo(MassiveCargo cargo){
        super(cargo);
    }

}
