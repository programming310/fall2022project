package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;

public class Compartment {
    private double currentWeight;
    private List<Container> listOfContainers;

    /**
     * Constructor for Compartment
     * @param ContainersPerComp
     */
    public Compartment(int ContainersPerComp) {
        List<Container> ContainerList = new ArrayList<Container>();

        for (int i = 0; i < ContainersPerComp; i++) {
            ContainerList.add(new Container());
        }
        this.listOfContainers = ContainerList;
        this.currentWeight = 0;
    }

    /**Copy constructor for compartment */
    public Compartment(Compartment other) {
        this(other.listOfContainers.size());
    }

    /**
     * Updates the current weight of the compartment by 
     * calculating the total weight of all cargo within it.
     */
    public void updateCurrentWeight() {
        double result = 0;
        for (Container c : this.listOfContainers) {
            result += c.getWeight();
        }
        this.currentWeight = result;
    }

    public double getCurrentWeight() {
        return this.currentWeight;
    }

    public List<Container> getListOfContainers() {
        return this.listOfContainers;
    }

    public int calculateNumberOfContainers() {
        return this.listOfContainers.size();
    }

    /**
     * Creates deep copy of a list of compartments
     * @param comps
     * @return list copy
     */
    public static List<Compartment> duplicateCompList(List<Compartment> comps){
        List<Compartment> copy = new ArrayList<Compartment>();
        for(Compartment c : comps){
            copy.add(new Compartment(c));
        }
        
        return copy;
    }
    
    /**
     * Optimizes space within a compartment using 
     * the next-fit decreasing algorithm
     * @param allCargo
     * @return list of remaining cargo after filling compartment
     */
    public List<Cargo> optimizeSpace(List<Cargo> allCargo) {
        for (int i = 0; i < this.listOfContainers.size(); i++) {
            while (this.listOfContainers.get(i).getIsOpen() && allCargo.size() > 0) {
                switch (allCargo.get(0).getClass().getSimpleName()) {
                    case "BoxedCargo":
                        if (this.listOfContainers.get(i).getSpaceFilled()
                                + allCargo.get(0).getVolume() < this.listOfContainers.get(i).getTotalVolume()) {
                            this.listOfContainers.get(i).addCargo(allCargo.get(0));
                            allCargo.remove(0);
                        } else {
                            this.listOfContainers.get(i).closeContainer();
                        }
                        break;
                    case "MassiveCargo":
                        if (this.listOfContainers.get(i).getSpaceFilled() == 0) {
                            this.listOfContainers.get(i).addCargo(allCargo.get(0));
                            allCargo.remove(0);
                            this.listOfContainers.get(i).closeContainer();
                        } else {
                            this.listOfContainers.get(i).closeContainer();
                        }
                        break;
                }
                updateCurrentWeight();
            }
        }
        updateCurrentWeight();
        return allCargo;
    }

    /**
     * Positions containers in order by destination
     * @param destinations
     * @return
     */
    public List<Container> optimizeContainerPositionning(String[] destinations) {
        List<Container> sortedArrContainer = new ArrayList<Container>();
        for (int i = 0; i < destinations.length; i++) {
            for (int j = 0; j < this.listOfContainers.size(); j++) {
                if (this.listOfContainers.get(j).getDestination().equals(destinations[i])) {
                    sortedArrContainer.add(this.listOfContainers.get(j));
                }
            }
        }
        return sortedArrContainer;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Compartment)) {
            return false;
        }
        Compartment comp = (Compartment) obj;

        for (int i = 0; i < comp.listOfContainers.size(); i++) {
            if (!(comp.listOfContainers.get(i).equals(this.listOfContainers.get(i)))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "\nCompartment: " + " currentWeight: " + this.currentWeight
                + " \tNumber Of Containers: "
                + this.listOfContainers.size();
    }
}
