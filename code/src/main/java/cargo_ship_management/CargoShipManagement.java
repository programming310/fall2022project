package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class CargoShipManagement extends Application {

    public static void main(String[] args) throws IOException, SQLException {
        Scanner reader = new Scanner(System.in);
        boolean valid = false;
        int userSelect = 0;
        while(!valid){
            try {
                System.out.println("How do you want to run the program?\n"
                + "1. In Terminal \t2. In Application");
                userSelect = reader.nextInt();
                if(userSelect != 1 && userSelect != 2)
                    throw new IllegalArgumentException();
                valid = true;
            } catch (Exception e) {
                System.out.println("Invalid Input! Try again please.");
            }
        }

        if(userSelect == 1){

            Boolean stillRunning = true;

            List<Boat> boats = new ArrayList<Boat>();
            List<Route> routes = new ArrayList<Route>();
            List<Cargo> cargo = new ArrayList<Cargo>();

            valid = false;
            while (!valid) {
                try {
                    System.out.println("How would you like to import data?\n1.By csv\t2.Through the database");
                    userSelect = reader.nextInt();
                    if (userSelect != 1 && userSelect != 2)
                        throw new IllegalArgumentException();
                    valid = true;
                } catch (Exception e) {
                    System.out.println("\nInvalid input. Please select '1' or '2'.\n");
                    reader.nextLine();
                }
            }
            switch (userSelect) {
                case 1:
                    boats = DataImporter.importShipsCSV("./data/ships.csv");
                    routes = DataImporter.importRoutesCSV("./data/routeList.csv");
                    cargo = DataImporter.importCargoCSV("./data/cargoList.csv");
                    break;
                case 2:
                    Connection conn = DataImporter.getConnection(reader);
                    boats = DataImporter.importShipsDB(conn);
                    routes = DataImporter.importRoutesDB(conn);
                    cargo = DataImporter.importCargoDB(conn);
                    conn.close();
                    break;
            }

            while (stillRunning) {
                stillRunning = runProgram(reader, Boat.duplicateBoatList(boats), Route.duplicateRouteList(routes), Cargo.duplicateCargoList(cargo));
            }
        }
        else if(userSelect == 2){System.out.println("Launching application...");
            Application.launch(args);            
        }
        System.out.println("\nGoodbye!\n");
        reader.close();
    }

    /**
     * Contains logic for running the program.
     * 
     * @throws IOException
     * @throws SQLException
     */
    public static boolean runProgram(Scanner reader, List<Boat> boats, List<Route> routes, List<Cargo> cargo)
            throws IOException, SQLException {
        int userSelect = 0;
        boolean valid = false;
        while (!valid) {
            try {
                System.out.println("\nBy which measure would you like to optimize the cargo?\n" +
                        "1.For space\t2.For time\t3.For easy unpacking");
                userSelect = reader.nextInt();
                if (userSelect <= 0 || userSelect > 4)
                    throw new IllegalArgumentException();
                valid = true;
            } catch (Exception e) {
                System.out.println("\nInvalid input. Please try again.\n");
                reader.nextLine();
            }
        }
        List<Boat> optimizedBoats = new ArrayList<Boat>();
        Boat optimalConfiguration;
        optimalConfiguration = boats.get(0);
        switch (userSelect) {
            case 1:
                for (Boat b : boats) {
                    if (b.optimizeForSpace(Cargo.duplicateCargoList(cargo)))
                        optimizedBoats.add(b);
                }
                optimalConfiguration = optimizedBoats.get(0);
                for (Boat b : optimizedBoats) {
                    if (b.countUnusedContainers() < optimalConfiguration.countUnusedContainers())
                        optimalConfiguration = b;
                   
                }
                break;
            case 2:
                for (Boat b : boats) {
                    b.optimizeForSpace(Cargo.duplicateCargoList(cargo));
                }
                optimalConfiguration = optimizeForTime(boats, routes);
                break;
            case 3:
                for (int i = 0; i < boats.size(); i++) {
                    boats.get(i).optimizeDestination(routes.get(i).getDestinations(),
                            Cargo.duplicateCargoList(cargo));
                    optimizedBoats.add(boats.get(i));
                }
                optimalConfiguration = optimizeForTime(boats, routes);
                break;
        }
        String printBoat = optimalConfiguration.toString();
        printBoat = printBoat.substring(1, printBoat.length() - 1);
        System.out.println("\n\nHere is your optimal configuration:\n" + printBoat);

        valid = false;
        boolean stillRunning = false;
        while (!valid) {
            try {
                System.out.println("\nWould you like to run another optimization?\n1.Yes\t2.No");
                userSelect = reader.nextInt();
                if (userSelect != 1 && userSelect != 2)
                    throw new IllegalArgumentException();
                if (userSelect == 1)
                    stillRunning = true;
                valid = true;
            } catch (Exception e) {
                System.out.println("\nInvalid input. Please try again.");
                reader.nextLine();
            }
        }
        return stillRunning;
    }

   
    /**
     * Given list of boats, finds the one that can complete
     * the given routes the fastest
     * @param boats
     * @param routes
     * @return fastest boat
     */
    public static Boat optimizeForTime(List<Boat> boats, List<Route> routes) {
        Boat fastestBoat = boats.get(0);
        double fastestTime = boats.get(0).calculateTime(routes);
        double time;
        for (Boat b : boats) {
            time = b.calculateTime(routes);
            if (time > fastestTime) {
                fastestBoat = b;
                fastestTime = time;
            }
        }

        return fastestBoat;
    }

    /**
     * Begins javafx
     */
    public void start(Stage stage) {

        stage.setTitle("Cargo Ship Management");

        javafxManager.setSceneImport(stage);

        stage.show();

    }
}

    