package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class javafxManager {

    /**
     * Sets the scene for importing data
     * @param stage
     */
    public static void setSceneImport(Stage stage) {

        Group root = new Group();

        Scene dataImport = new Scene(root, 900, 500);
        dataImport.setFill(Color.BLACK);

        VBox overall = new VBox();
        overall.setMinWidth(900);
        overall.setMinHeight(500);

        VBox vbox1 = new VBox();
        vbox1.setStyle("-fx-alignment: center");

        Text text1 = new Text("\n\nWelcome to Cargo Ship Management!\n\n ");
        text1.setFill(Color.WHITE);
        text1.setFont(new Font(40));

        Text text2 = new Text("How would you like to import the data?\n ");
        text2.setFill(Color.WHITE);
        text2.setFont(new Font(30));

        vbox1.getChildren().addAll(text1, text2);

        HBox hbox2 = new HBox();
        hbox2.setStyle("-fx-alignment: center");

        Button csv = new Button("By csv");
        csv.setTranslateX(-15);
        csv.setFont(new Font(20));

        Button database = new Button("Through the database");
        database.setTranslateX(15);
        database.setFont(new Font(20));

        hbox2.getChildren().addAll(csv, database);

        overall.getChildren().addAll(vbox1, hbox2);
        root.getChildren().add(overall);

        csv.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                try {
                    List<Boat> boats = DataImporter.importShipsCSV("./data/ships.csv");
                    List<Route> routes = DataImporter.importRoutesCSV("./data/routeList.csv");
                    List<Cargo> allCargo = DataImporter.importCargoCSV("./data/cargoList.csv");

                    setSceneChoice(stage, boats, routes, allCargo);
                } catch (IOException error) {
                    System.out.println(error.getMessage());
                }
            }

        });

        database.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                setSceneDBlogin(stage);
            }

        });

        stage.setScene(dataImport);
    }

    /**
     * Sets the scene for logging into the database
     * @param stage
     */
    public static void setSceneDBlogin(Stage stage) {

        Group root = new Group();

        Scene dbLogin = new Scene(root, 900, 500);
        dbLogin.setFill(Color.BLACK);

        VBox overall = new VBox();
        overall.setMinWidth(900);
        overall.setMinHeight(500);

        HBox hbox1 = new HBox();
        hbox1.setStyle("-fx-alignment: center");
        hbox1.setTranslateY(100);

        Text username = new Text("Username :\t ");
        username.setFill(Color.WHITE);
        username.setFont(new Font(20));
        TextField usernameInput = new TextField();

        hbox1.getChildren().addAll(username, usernameInput);

        HBox hbox2 = new HBox();
        hbox2.setStyle("-fx-alignment: center");
        hbox2.setTranslateY(150);

        Text password = new Text("Password :\t ");
        password.setFill(Color.WHITE);
        password.setFont(new Font(20));
        PasswordField passwordInput = new PasswordField();

        hbox2.getChildren().addAll(password, passwordInput);

        VBox vbox1 = new VBox();
        vbox1.setStyle("-fx-alignment: center");
        vbox1.setTranslateY(250);

        Text errorMsg = new Text();
        errorMsg.setFill(Color.RED);
        errorMsg.setFont(new Font(15));

        Button login = new Button("Login");
        login.setFont(new Font(20));
        login.setTranslateY(20);

        vbox1.getChildren().addAll(errorMsg, login);

        overall.getChildren().addAll(hbox1, hbox2, vbox1);
        root.getChildren().add(overall);

        login.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                String user = usernameInput.getText();
                String pwd = passwordInput.getText();

                try {
                    Connection conn = DataImporter.getConnection(user, pwd);
                    List<Boat> boats = DataImporter.importShipsDB(conn);
                    List<Route> routes = DataImporter.importRoutesDB(conn);
                    List<Cargo> allCargo = DataImporter.importCargoDB(conn);

                    setSceneChoice(stage, boats, routes, allCargo);
                } catch (Exception error) {
                    errorMsg.setText("Invalid username/password. Please try again.");
                }

            }
        });

        stage.setScene(dbLogin);

    }

    /**
     * Sets the scene for displaying imported data and choosing how to optimize
     * @param stage
     * @param boats
     * @param routes
     * @param allCargo
     */
    public static void setSceneChoice(Stage stage, List<Boat> boats, List<Route> routes, List<Cargo> allCargo) {

        Group root = new Group();

        Scene chooseOptimization = new Scene(root, 900, 500);
        chooseOptimization.setFill(Color.BLACK);

        VBox overall = new VBox();
        overall.setMinWidth(900);
        overall.setMinHeight(500);

        VBox top = new VBox();
        top.setStyle("-fx-border-color: white; -fx-alignment: center");

        Text title = new Text("How would you like to optimize the cargo?\n ");
        title.setTranslateY(10);
        title.setTranslateX(5);
        title.setFill(Color.WHITE);
        title.setFont(new Font(20));

        HBox buttons = new HBox();
        buttons.setTranslateY(-5);
        buttons.setStyle("-fx-alignment: center");
        Button b1 = new Button("For space");
        b1.setFont(new Font(15));
        b1.setTranslateX(-50);
        Button b2 = new Button("For time");
        b2.setFont(new Font(15));
        Button b3 = new Button("For easy unpacking");
        b3.setFont(new Font(15));
        b3.setTranslateX(50);
        buttons.getChildren().addAll(b1, b2, b3);

        top.getChildren().addAll(title, buttons);

        HBox data = new HBox();

        ScrollPane scrollCargo = new ScrollPane();
        scrollCargo.setPrefSize(450,410);
        scrollCargo.fitToWidthProperty().set(true);
        scrollCargo.fitToHeightProperty().set(true);
        scrollCargo.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        VBox cargoList = new VBox();
        cargoList.setStyle("-fx-background-color: black; -fx-border-color: white");
        Text cargoTitle = new Text("List of Cargo: ");
        cargoTitle.setFill(Color.WHITE);
        cargoTitle.setFont(new Font(20));
        cargoList.getChildren().add(cargoTitle);
        Text currentCargo;
        for(Cargo c : allCargo){
            currentCargo = new Text(" " + c.getGoodsType() + "\n\tType: " + c.getClass().getSimpleName() + 
            ", Weight: " + Math.round(c.getWeight()*1000)/1000.0 + ", Volume: " + Math.round(c.getVolume()*100)/100.0 +
            ", Dest: " + c.getDestination());
            currentCargo.setFill(Color.WHITE);
            currentCargo.setFont(new Font(13));
            cargoList.getChildren().add(currentCargo);
        }
        scrollCargo.setContent(cargoList);

        VBox rightSide = new VBox();

        ScrollPane scrollBoats = new ScrollPane();
        scrollBoats.setPrefSize(450, 205);
        scrollBoats.fitToWidthProperty().set(true);
        scrollBoats.fitToHeightProperty().set(true);
        scrollBoats.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        VBox boatList = new VBox();
        boatList.setStyle("-fx-background-color: black;-fx-border-color: white");
        Text boatTitle = new Text("List of Boats: ");
        boatTitle.setFill(Color.WHITE);
        boatTitle.setFont(new Font(20));
        boatList.getChildren().add(boatTitle);
        Text currentBoat;
        for (Boat b : boats) {
            currentBoat = new Text("  " + b.getName() + ":\n    Type: " + b.getClass().getSimpleName() + ", Capacity: "
                    + b.getWeightCapacity() + " DWT, Compartments: " + b.getCompartments().size()
                    + ", Total containers: "
                    + b.getCompartments().size() * b.getCompartments().get(0).getListOfContainers().size());
            currentBoat.setFill(Color.WHITE);
            currentBoat.setFont(new Font(12));
            boatList.getChildren().add(currentBoat);
        }
        scrollBoats.setContent(boatList);

        ScrollPane scrollRoutes = new ScrollPane();
        scrollRoutes.setPrefSize(450, 205);
        scrollRoutes.fitToWidthProperty().set(true);
        scrollRoutes.fitToHeightProperty().set(true);
        scrollRoutes.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        VBox routeList = new VBox();
        routeList.setStyle("-fx-background-color: black;-fx-border-color: white");
        Text routeTitle = new Text("List of Routes: ");
        routeTitle.setFill(Color.WHITE);
        routeTitle.setFont(new Font(20));
        routeList.getChildren().add(routeTitle);
        Text currentRoute;
        String destinations;
        for (Route r : routes) {
            destinations = r.getDestinations()[0];
            for (int i = 1; i < r.getDestinations().length; i++) {
                destinations += ", " + r.getDestinations()[i];
            }
            currentRoute = new Text("  Route " + r.getRouteId() + ", Total distance: " + r.getTotalDistance() +
             "km \n\tdestinations: " + destinations);
            currentRoute.setFill(Color.WHITE);
            currentRoute.setFont(new Font(12));
            routeList.getChildren().add(currentRoute);
        }
        scrollRoutes.setContent(routeList);

        rightSide.getChildren().addAll(scrollBoats, scrollRoutes);
        data.getChildren().addAll(scrollCargo, rightSide);

        overall.getChildren().addAll(top, data);
        root.getChildren().add(overall);

        b1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                List<Boat> optimizedBoats = new ArrayList<Boat>();
                Boat optimalConfiguration;

                optimalConfiguration = boats.get(0);
                for (Boat b : boats) {
                    if (b.optimizeForSpace(Cargo.duplicateCargoList(allCargo)))
                        optimizedBoats.add(b);
                }
                optimalConfiguration = optimizedBoats.get(0);
                for (Boat b : optimizedBoats) {
                    if (b.countUnusedContainers() < optimalConfiguration.countUnusedContainers())
                        optimalConfiguration = b;
                }

                setSceneResult(stage, optimalConfiguration, routes, 1);
            }

        });

        b2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                Boat optimalConfiguration;
                for (Boat b : boats) {
                    b.optimizeForSpace(Cargo.duplicateCargoList(allCargo));
                }
                optimalConfiguration = CargoShipManagement.optimizeForTime(boats, routes);

                setSceneResult(stage, optimalConfiguration, routes, 1);
            }

        });

        b3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                Boat optimalConfiguration;
                List<Boat> optimizedBoats = new ArrayList<Boat>();
                for (int i = 0; i < boats.size(); i++) {
                    boats.get(i).optimizeDestination(routes.get(i).getDestinations(),
                            Cargo.duplicateCargoList(allCargo));
                    optimizedBoats.add(boats.get(i));
                }
                optimalConfiguration = CargoShipManagement.optimizeForTime(boats, routes);

                setSceneResult(stage, optimalConfiguration, routes, 1);
            }

        });

        stage.setScene(chooseOptimization);

    }

    /**
     * Sets the scene for displaying the result of optimization
     * @param stage
     * @param optimalConfiguration
     * @param routes
     * @param compNum
     */
    public static void setSceneResult(Stage stage, Boat optimalConfiguration, List<Route> routes, int compNum){
        Group root = new Group();
        Scene scene = new Scene(root, 900, 500);
        scene.setFill(Color.BLACK);

        VBox overall = new VBox();
        overall.setMinWidth(900);
        overall.setMinHeight(500);

        VBox top = new VBox();
        top.setStyle("-fx-alignment: center; -fx-border-color: white");

        Text title = new Text("\nHere is your optimal configuration: ");
        title.setFill(Color.WHITE);
        title.setFont(new Font(20));
        title.setTranslateY(-5);

        Text optimalBoatText = new Text("\nBoat: " + optimalConfiguration.getName() + 
        "      Type: " + optimalConfiguration.getClass().getSimpleName() + "      Total deadweight: " 
        + Math.round(optimalConfiguration.getTotalWeight()*100)/100.0 + " tons      Travel time: " 
        + Math.round(optimalConfiguration.calculateTime(routes)*100)/100.0 + " hours");
        optimalBoatText.setFill(Color.WHITE);
        optimalBoatText.setFont(new Font(15));
        optimalBoatText.setTranslateY(-5);
        top.getChildren().addAll(title, optimalBoatText);


        VBox middle = new VBox();
        
        HBox compSelect = new HBox();

        compSelect.setStyle("-fx-alignment: center; -fx-border-color: white");
        compSelect.setPadding(new Insets(10));
        Button select;
        for(int i = 0; i < optimalConfiguration.getCompartments().size(); i++){
            select = new Button("Compartment " + (i+1));
            select.setFont(new Font(15));
            select.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent e){
                    String[] target = e.getTarget().toString().split("\'");
                    String compNum = target[target.length-1].split(" ")[1];

                    setSceneResult(stage, optimalConfiguration, routes, Integer.parseInt(compNum));
                }
            });

            compSelect.getChildren().add(select);
        }

        ScrollPane compContents = new ScrollPane();
        compContents.setPrefSize(900, 310);
        compContents.fitToWidthProperty().set(true);
        compContents.fitToHeightProperty().set(true);
        compContents.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        VBox contents = new VBox();
        contents.setStyle("-fx-background-color: black;-fx-border-color: white");

        Text compTitle = new Text("Listing contents of compartment " + compNum + ": ");
        compTitle.setFill(Color.WHITE);
        compTitle.setFont(new Font(20));
        contents.getChildren().add(compTitle);
        Text currContText;
        Container currCont;
        Text currCargoText;
        Cargo currCargo;
        for(int i = 0; i < optimalConfiguration.getCompartments().get(compNum-1).getListOfContainers().size(); i++){
            currCont = optimalConfiguration.getCompartments().get(compNum-1).getListOfContainers().get(i);
            currContText = new Text("\tContainer " + (i+1));
            currContText.setFill(Color.WHITE);
            currContText.setFont(new Font(15));
            contents.getChildren().add(currContText);

            for(int j = 0; j < currCont.getListOfCargo().size(); j++){
                currCargo = currCont.getListOfCargo().get(j);
                currCargoText = new Text("\t\t" + (j+1) + ". " + currCargo.getGoodsType() +
                ", Type: " + currCargo.getClass().getSimpleName() + ", Weight: " + 
                Math.round(currCargo.getWeight()*1000.0)/1000.0 + ", Volume: " +
                Math.round(currCargo.getVolume()*100.0)/100.0 + ", Destination: " + currCargo.getDestination());
                currCargoText.setFill(Color.WHITE);
                currCargoText.setFont(new Font(13));
                contents.getChildren().add(currCargoText);
            }

        }

        compContents.setContent(contents);

        middle.getChildren().addAll(compSelect, compContents);


        HBox bottom = new HBox();
        bottom.setStyle("-fx-alignment: center; -fx-border-color: white");
        bottom.setPadding(new Insets(5));

        Text restart = new Text("Would you like to run another optimization?   ");
        restart.setFill(Color.WHITE);
        restart.setFont(new Font(18));

        Button b1 = new Button("Yes");
        Button b2 = new Button("No");
        b2.setTranslateX(10);

        bottom.getChildren().addAll(restart, b1, b2);

        overall.getChildren().addAll(top, middle, bottom);
        root.getChildren().add(overall);


        b1.setOnAction(new EventHandler<ActionEvent>() {   
            @Override
            public void handle(ActionEvent e){
                setSceneImport(stage);
            }
        });

        b2.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e){
                stage.close();
            }  
        });

        stage.setScene(scene);

    }
}

