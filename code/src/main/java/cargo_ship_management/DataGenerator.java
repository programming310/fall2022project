package cargo_ship_management;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class DataGenerator {

    /**
     * Generates a randomized set of cargo data and adds it to cargoList.csv and database
     * @param args
     * @throws IOException
     * @throws SQLException
     */
    public static void main(String[] args) throws IOException, SQLException {
        Connection conn = getConnection();

        List<String> randomizedData = new ArrayList<String>();
        String type = "";
        String name = "";
        int amount = 1;
        double weight = 1.0;
        double space = 1.0;
        String destination = "";

        for (int i = 0; i < 100; i++) {
            type = getType();
            name = getName(type);
            weight = getWeight(type);
            space = getSpace(type);
            amount = getAmount(type, space);
            destination = getDestination();

            randomizedData.add(type + "," + name + "," + amount + "," + weight + "," + space + "," + destination);

            addToDB(conn, type, name, amount, weight, space, destination);
        }

        Path p = Paths.get("data/cargoList.csv");
        Files.write(p, randomizedData, StandardOpenOption.APPEND);

        System.out.print("All done!");
    }

    /**
     * Gets a random name from list
     * @param type
     * @return name String
     */
    public static String getName(String type) {
        final String[] boxedCargoTypes = new String[] { "Phones", "Chair", "Table", "Tablet",
                "Printer", "Television", "Xbox", "Playstation", "Wood", "Car parts",
                "Graphics Card", "Sweater", "Coat", "Shoes", "Pants", "Lego", "Toy",
                "Iron", "Steel", "Pills", "Medicine", "Produce", "Sporting equipment",
                "Water bottle", "Alcohol", "Tools", "Paper", "Book", "Wheel",
                "Hard drive", "Mail", "Painting", "Sculpture", "Lamp", "Shovel", "Gold" };

        final String[] massiveCargoTypes = new String[] { "Car", "Motorcycle", "Pickup Truck", "Forklift",
                "Industrial Refrigerator", "Metal Beam", "Sports Car", "Large Cement block", "Boat", "King Size Bed",
                "Heavy equipment", "Specialized Cargo", "Steamroller", "Pool Table", "Industrial Deep Fryer",
                "Trailer", "Boulder", "Couch", "Tractor", "Jetski" };

        String result = "";
        Random r = new Random();
        switch (type) {
            case "BoxedCargo":
                result = boxedCargoTypes[r.nextInt(boxedCargoTypes.length)];
                break;
            case "MassiveCargo":
                result = massiveCargoTypes[r.nextInt(massiveCargoTypes.length)];
                break;
        }
        return result;
    };

    /**
     * Generates random volume for cargo between specific ranges
     * @param type
     * @return volume
     */
    private static double getSpace(String type) {
        double space = 0;
        switch (type) {
            case "MassiveCargo":
                space = randomDouble(16.0, 32.0);
                break;
            case "BoxedCargo":
                space = randomDouble(0.0, 8.0);
                break;
        }

        if (space < 2)
            return Math.round(space * 100000.0) / 100000.0;
        else
            return Math.round(space * 100.0) / 100.0;
    }

    /**
     * Generates random destination from list
     * @return destination String
     */
    private static String getDestination() {
        String[] destinations = new String[] { "Iceland", "Sweden", "Norway", "Finland", "Russia", "Usa", "Canada",
                "Greenland", "Portugal", "Spain", "France", "Italy", "Grece", "Turkey", "Egypt" };
        Random r = new Random();

        return destinations[r.nextInt(destinations.length)];
    }

    /**
     * Picks random cargo type between BoxedCargo and MassiveCargo
     * @return type String
     */
    private static String getType() {
        Random r = new Random();
        int num = r.nextInt(2);
        String type = "";
        switch (num) {
            case 0:
                type = "BoxedCargo";
                break;
            case 1:
                type = "MassiveCargo";
                break;
        }

        return type;
    }

    /**
     * Generates random amount of items for BoxedCargo
     * @param type
     * @param space
     * @return amount int
     */
    public static int getAmount(String type, double space) {
        Random r = new Random();
        int result = 1;
        if (type == "BoxedCargo")
            result = r.nextInt((int) Math.round(32 / space));
        else if (type == "MassiveCargo")
            return 1;
        
        if(result != 0)
            return result;
        return 1;
    }

    /**
     * Generates random weight between specific ranges for cargo
     * @param type
     * @return weight double
     */
    public static double getWeight(String type) {
        double weight = 0;
        switch (type) {
            case "MassiveCargo":
                weight = randomDouble(500.0, 5000.0);
                break;
            case "BoxedCargo":
                weight = randomDouble(0.0, 50.0);
                break;
        }
        return Math.round(weight * 100.0) / 100.0;
    }

    /**
     * Establishes a connection to pdbora19c
     * @return Connection
     */
    public static Connection getConnection() {
        Scanner reader = new Scanner(System.in);
        System.out.println("Username: ");
        String user = reader.nextLine();
        System.out.println("Password: ");
        String pass = reader.nextLine();
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            return conn;
        } catch (Exception e) {
            System.out.println(e.getMessage() + "\nThere was an error!");
        }
        reader.close();
        return null;
    }

    /**
     * Generates a random double between given range
     * @param min
     * @param max
     * @return double
     */
    public static double randomDouble(double min, double max) {
        Random r = new Random();
        return r.nextDouble() * (max - min) + min;
    }

    /**Inserts a cargo into the database */
    public static void addToDB(Connection conn, String type, String name, int amount, double weight, double space,
            String destination) throws SQLException {
        try {
            String sql = "INSERT INTO cargoList VALUES('" + type + "', '" + name +
                    "', " + amount + ", " + weight + ", " + space + ", '" + destination + "')";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println("There was an error!");
            System.out.println(e.getMessage());
        }
    }
}