package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;

public abstract class Boat {
    private String name;
    private double weightCapacity;
    private double topSpeed;
    private List<Compartment> compartments;

    /**
     * Constructor for boat
     * @param name
     * @param weightCapacity
     * @param numCompartments
     * @param ContainersPerComp
     * @param topSpeed
     */
    public Boat(String name, double weightCapacity, int numCompartments, int ContainersPerComp,
            double topSpeed) {
        this.name = name;
        this.weightCapacity = weightCapacity;
        this.topSpeed = topSpeed;

        List<Compartment> compartments = new ArrayList<Compartment>();
        for (int i = 0; i < numCompartments; i++) {
            compartments.add(new Compartment(ContainersPerComp));
        }

        this.compartments = compartments;
    }

    /**
     * Copy constructor for Boat
     * @param b
     */
    public Boat(Boat b){
        this.name = b.getName();
        this.weightCapacity = b.getWeightCapacity();
        this.topSpeed = b.getTopSpeed();
        this.compartments = Compartment.duplicateCompList(b.getCompartments());
    }

    /**
     * Duplicates a list of boats
     * @param allBoats
     * @return duplicated list
     */
    public static List<Boat> duplicateBoatList(List<Boat> allBoats){
        List<Boat> copy = new ArrayList<Boat>();
        for(Boat b : allBoats){
            switch (b.getClass().getSimpleName()) {
                case "Feeders":
                    copy.add(new Feeders((Feeders)b));
                    break;
                case "Icebreaker":
                    copy.add(new Icebreaker((Icebreaker)b));
                    break;
                case "International":
                    copy.add(new International((International)b));
                    break;
            }
        }
        return copy;
    }


    public String getName() {
        return this.name;
    }

    public double getWeightCapacity() {
        return this.weightCapacity;
    }

    public double getTopSpeed() {
        return this.topSpeed;
    }

    public List<Compartment> getCompartments() {
        return this.compartments;
    }

    /**
     * Optimizes the cargo in reverse order of array of destinations
     * @param destinations
     * @param allCargo
     */
    public void optimizeDestination(String[] destinations, List<Cargo> allCargo) {
        double compartmentTotalWeight = 0;
        int numOfContainersAvailable = 0;
        allCargo = Container.sortCargoDest(destinations, allCargo);
        try {
            for (int i = 0; i < this.compartments.size(); i++) {
                numOfContainersAvailable += this.compartments.get(i).calculateNumberOfContainers();
                for (int j = 0; j < this.compartments.get(i).getListOfContainers().size(); j++) {
                    allCargo = Cargo
                            .duplicateCargoList(
                                    this.compartments.get(i).getListOfContainers().get(j).addCargo(allCargo));
                    this.compartments.get(i).updateCurrentWeight();

                    compartmentTotalWeight += this.compartments.get(i).getListOfContainers().get(j)
                            .calculateWeightContainer();
                    // Throws error if there is no more containers available
                    if (numOfContainersAvailable > this.compartments.size()
                            * this.compartments.get(i).calculateNumberOfContainers()) {
                        throw new IllegalArgumentException("ERROR! There is not enough container");
                    }
                    // Throws error if weight capacity exceeded
                    if (compartmentTotalWeight >= this.weightCapacity) {
                        throw new IllegalArgumentException("ERROR! The weight of your cargo is exceeded");
                    }
                    // Verify that the cargo list is empty before exiting the nested loop
                    if (allCargo.size() == 0) {
                        break;
                    }
                }
                // Verify that the cargo list is empty before exiting the loop
                if (allCargo.size() == 0) {
                    break;
                }
                this.compartments.get(i).updateCurrentWeight();
                this.compartments.get(i).optimizeContainerPositionning(destinations);
            }
        } catch (Exception e) {
        }
    }

    /**
     * Gets the total weight of all cargo on the boat
     * @return
     */
    public double getTotalWeight() {
        double currWeight = 0;
        for (int i = 0; i < this.compartments.size(); i++) {
            this.compartments.get(i).updateCurrentWeight();
            currWeight += this.compartments.get(i).getCurrentWeight();
        }
        return currWeight;
    }

    /**
     * Given list of Cargo, this method will optimally fills containers
     * one compartment at a time using the Next-Fit-Decreasing (NFD)
     * algorithm of the bin packing problem.
     * 
     * @param allCargo
     */
    public boolean optimizeForSpace(List<Cargo> allCargo) {
        allCargo = Cargo.sortDecreasing(allCargo);
        for (int i = 0; i < this.compartments.size(); i++) {
            allCargo = this.compartments.get(i).optimizeSpace(allCargo);
        }
        if (allCargo.size() == 0)
            return true;
        else
            return false;
    }

    /**
     * Calculates speed of boat based on top speed
     * and total weight
     * @return  calculated speed
     */
    public double calculateSpeed() {
        double slowdownPercentage = (this.getTotalWeight() / this.weightCapacity) * 0.5;

        return this.topSpeed * (1.0 - slowdownPercentage);
    }

    /**
     * Calculates the time needed to complete a list of routes
     * @param routes
     * @return the calculated time
     */
    public double calculateTime(List<Route> routes) {
        double totalDistance = 0;
        for (Route r : routes) {
            if (r.isCanals() && this instanceof Feeders) {
                totalDistance += ((Feeders) this).passCanal(r.getTotalDistance());
            } else if (r.isIcy() && this instanceof Icebreaker) {
                totalDistance += ((Icebreaker) this).breakIce(r.getTotalDistance());
            } else if (r.isOcean() && this instanceof International) {
                totalDistance += ((International) this).crossOcean(r.getTotalDistance());
            } else {
                totalDistance += r.getTotalDistance();
            }
        }

        return totalDistance / this.calculateSpeed();
    }

    /**
     * Counts how many containers are open
     * @return number of open containers
     */
    public int countUnusedContainers() {
        int total = this.compartments.size() * this.compartments.get(0).getListOfContainers().size();
        int count = 0;
        for (Compartment comp : this.compartments) {
            for (Container cont : comp.getListOfContainers()) {
                if (cont.getIsOpen()) {
                    count++;
                }
            }
        }
        return total - count;
    }

    @Override
    public String toString() {
        String compartmentsDisplay = this.compartments.toString();
        compartmentsDisplay = compartmentsDisplay.substring(1, compartmentsDisplay.length() - 1);
        return "\n Boat Name: " + this.name + "\n Weight Capacity: " + this.weightCapacity
                + "\n Speed: " + this.topSpeed + "km/h" + "\n\n compartments: " + compartmentsDisplay;
    }
}
