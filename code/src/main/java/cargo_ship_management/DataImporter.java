package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import java.io.IOException;
import java.nio.file.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataImporter {
    
    /**
     * Creates a connection to the pdbora19c database.
     * @param reader
     * @return Connection
     */
    public static Connection getConnection(Scanner reader){
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        System.out.println("Username:");
        String username = reader.next();
        System.out.println("Password:");
        String password = reader.next();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            return conn;
        }
        catch(SQLException e) {
            System.out.println("Connection failed.");
        }

        return null;
    }


    /**
     * Creates connection to db with given inputs for username and password
     * @param username
     * @param password
     * @return Connection
     */
    public static Connection getConnection(String username, String password){
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            return conn;
        }
        catch(SQLException e) {
            System.out.println("Connection failed.");
        }
        return null;
    }

    /**
     * Converts a varchar from the database to a boolean value
     * @param varchar
     * @return boolean
     */
    public static boolean convertDBtoBoolean(String varchar){
        if(varchar == "x")
            return true;
        else
            return false;
    }

    /**
     * Creates a list of boat objects from values retrieved from the database.
     * @param conn
     * @return List<Boat>
     * @throws SQLException
     */
    public static List<Boat> importShipsDB(Connection conn) throws SQLException{
        List<Boat> boats = new ArrayList<Boat>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ships");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                switch(rs.getString(1)){
                    case "Icebreaker":
                    boats.add(new Icebreaker(rs.getString(2), rs.getDouble(3),
                     rs.getInt(4), rs.getInt(5), rs.getDouble(6)));
                    break;
                    case "International":
                    boats.add(new International(rs.getString(2), rs.getDouble(3),
                     rs.getInt(4), rs.getInt(5), rs.getDouble(6)));
                    break;
                    case "Feeders":
                    boats.add(new Feeders(rs.getString(2), rs.getDouble(3),
                     rs.getInt(4), rs.getInt(5), rs.getDouble(6)));
                    break;
                }
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return boats;
    }

    /**
     * Creates list of boats from a csv file.
     * @param path
     * @return  List<Boat>
     * @throws IOException
     */
    public static List<Boat> importShipsCSV(String path) throws IOException{
        List<Boat> allShips = new ArrayList<Boat>();
        try {
            String[] pieces;
            Path p = Paths.get(path);
            List<String> lines = Files.readAllLines(p);

            for(int i = 1; i < lines.size(); i++){
                pieces = lines.get(i).split(",");
                switch (pieces[0]) {
                    case "International":
                        allShips.add(new International(pieces[1], Double.valueOf(pieces[2]),
                                Integer.valueOf(pieces[3]), Integer.valueOf(pieces[4]), Double.valueOf(pieces[5])));
                        break;
                    case "Icebreaker":
                        allShips.add(new Icebreaker(pieces[1], Double.valueOf(pieces[2]),
                                Integer.valueOf(pieces[3]), Integer.valueOf(pieces[4]), Double.valueOf(pieces[5])));
                        break;
                    case "Feeder":
                        allShips.add(new Feeders(pieces[1], Double.valueOf(pieces[2]),
                                Integer.valueOf(pieces[3]), Integer.valueOf(pieces[4]), Double.valueOf(pieces[5])));
                }
            }
        } 
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return allShips;
    }

    /**
     * Retrieves a list of routes from the database.
     * @param conn
     * @return List<Route>
     * @throws SQLException
     */
    public static List<Route> importRoutesDB(Connection conn) throws SQLException {
        List<Route> allRoutes = new ArrayList<Route>();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM routeList");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                allRoutes.add(new Route(rs.getInt(1), rs.getString(2).split(":"), 
                rs.getDouble(3), convertDBtoBoolean(rs.getString(4)), 
                convertDBtoBoolean(rs.getString(5)), convertDBtoBoolean(rs.getString(6))));
            }
            return allRoutes;
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * Retrieves a list of routes from a csv file.
     * @param filename
     * @return List<Route>
     * @throws IOException
     */
    public static List<Route> importRoutesCSV(String filename) throws IOException {
        Path p = Paths.get(filename);
        List<String> lines = Files.readAllLines(p);
        List<Route> routes = new ArrayList<Route>();
        try {
            for (int i = 1; i < lines.size(); i++) {
                String[] pieces = lines.get(i).split(",");

                routes.add(new Route(Integer.parseInt(pieces[0]), pieces[1].split(":"), Double.parseDouble(pieces[2]),
                        Boolean.parseBoolean(pieces[3]), Boolean.parseBoolean(pieces[4]),
                        Boolean.parseBoolean(pieces[5])));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    /**
     * Retrieves a list of cargo from the database.
     * @param conn
     * @return List<Cargo>
     * @throws SQLException
     */
    public static List<Cargo> importCargoDB(Connection conn) throws SQLException {
        List<Cargo> allCargo = new ArrayList<Cargo>();

        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM cargoList");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                switch(rs.getString(1)){
                    case "MassiveCargo":
                    allCargo.add(new MassiveCargo(rs.getString(2), rs.getDouble(4),
                     rs.getDouble(5), rs.getString(6)));
                    break;
                    case "BoxedCargo":
                    allCargo.add(new BoxedCargo(rs.getString(2), rs.getInt(3), 
                    rs.getDouble(4), rs.getDouble(5), rs.getString(6)));
                    break;
                }
            }
            return allCargo;
        }
        catch(SQLException e) {
            System.out.print(e.getMessage());
        }

        return null;
    }

    /**
     * Retrieves a list of cargo from a csv file.
     * @param filename
     * @return List<Cargo>
     * @throws IOException
     */
    public static List<Cargo> importCargoCSV(String filename) throws IOException {
        List<Cargo> allCargo = new ArrayList<Cargo>();

        try {
            String[] pieces;
            Path p = Paths.get(filename);
            List<String> lines = Files.readAllLines(p);
            for (String line : lines){
                pieces = line.split(",");
                switch(pieces[0]){
                    case "BoxedCargo":
                    allCargo.add(new BoxedCargo(pieces[1], Integer.valueOf(pieces[2]), 
                        Double.valueOf(pieces[3]), Double.valueOf(pieces[4]), pieces[5]));
                    break;
                    case "MassiveCargo":
                    allCargo.add(new MassiveCargo(pieces[1], Double.valueOf(pieces[3]),
                        Double.valueOf(pieces[4]), pieces[5]));
                        break;
                }
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }

        return allCargo;
    }
}
