package cargo_ship_management;

public class International extends Boat{

    /**
     * Constructor for International
     * @param name
     * @param weightCapacity
     * @param numCompartments
     * @param containersPerComp
     * @param topSpeed
     */
    public International(String name, double weightCapacity, int numCompartments, int containersPerComp, double topSpeed) {
        super(name, weightCapacity, numCompartments, containersPerComp, topSpeed);
    }

    /**
     * Copy constructor
     * @param i
     */
    public International(International i){
        super(i);
    }

    /**
     * Reduces distance in international waters by easily crossing rough ocean waters
     * @param totalDistance
     * @return distance double
     */
    public double crossOcean(double totalDistance) {
        return totalDistance*0.85;
    }
    
}
