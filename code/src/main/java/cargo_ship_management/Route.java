package cargo_ship_management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Route {
    private int routeId;
    private String[] destinations;
    private double totalDistance;
    private boolean isIcy;
    private boolean isOcean;
    private boolean isCanals;

    /**
     * Constructor for route
     * @param routeId
     * @param destinations
     * @param totalDistance
     * @param isIcy
     * @param isOcean
     * @param isCanals
     */
    public Route(int routeId, String[] destinations, double totalDistance, boolean isIcy, boolean isOcean,
            boolean isCanals) {
        this.routeId = routeId;
        this.destinations = destinations;
        this.totalDistance = totalDistance;
        this.isIcy = isIcy;
        this.isOcean = isOcean;
        this.isCanals = isCanals;
    }

    /**
     * Copy constructor for route
     * @param r
     */
    public Route(Route r){
        this.routeId = r.getRouteId();
        this.destinations = new String[r.destinations.length];
        for(int i = 0; i < r.getDestinations().length; i++){
            this.destinations[i] = r.getDestinations()[i];
        }
        this.totalDistance = r.getTotalDistance();
        this.isIcy = r.isIcy();
        this.isOcean = r.isOcean;
        this.isCanals = r.isCanals;
    }

    public int getRouteId() {
        return routeId;
    }
    public String[] getDestinations() {
        return this.destinations;
    }

    public double getTotalDistance() {
        return this.totalDistance;
    }

    public boolean isIcy() {
        return this.isIcy;
    }

    public boolean isOcean() {
        return this.isOcean;
    }

    public boolean isCanals() {
        return this.isCanals;
    }

    /**
     * Creates deep copy of a list of routes
     * @param allRoutes
     * @return copy of route list
     */
    public static List<Route> duplicateRouteList(List<Route> allRoutes) {
        List<Route> copy = new ArrayList<Route>();
        for (Route r : allRoutes) {
            copy.add(new Route(r));
        }
        return copy;
    }

    @Override
    public String toString() {
        return "Route [routeId=" + routeId + ", destinations=" + Arrays.toString(destinations) + ", totalDistance="
                + totalDistance + ", isIcy=" + isIcy + ", isOcean=" + isOcean + ", isCanals=" + isCanals + "]";
    }

}
