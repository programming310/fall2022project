package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;

public class Container {

    private final double length = 6;
    private final double width = 2.34;
    private final double height = 2.28;
    private List<Cargo> listOfCargo;
    private double weight;
    private String destination;
    private boolean isOpen;

    /**
     * Constructor for container(All containers are the same initially)
     */
    public Container() {
        this.listOfCargo = new ArrayList<Cargo>();
        this.weight = 0;
        this.isOpen = true;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * Adds cargo to a container
     * @param listOfNewCargo
     * @return resulting list of cargo
     */
    public List<Cargo> addCargo(List<Cargo> listOfNewCargo) {
        double volume = 0;
        for (Cargo cargo : listOfNewCargo) {
            volume += cargo.getVolume();
            switch (cargo.getClass().getSimpleName()) {
                case "BoxedCargo":
                    if (getTotalVolume() > volume && getIsOpen()) {
                        this.listOfCargo.add(cargo);
                        this.weight += cargo.getWeight();
                        setDestination(this.listOfCargo.get(0).getDestination());
                    }
                    if (!this.listOfCargo.get(0).getDestination().equals(cargo.getDestination())) {
                        this.listOfCargo.remove(cargo);
                        closeContainer();
                    }
                    break;
                case "MassiveCargo":
                    if(!(this.listOfCargo.size() == 0)) {
                        closeContainer();
                    }
                    if (getTotalVolume() > volume && getIsOpen()) {
                        setDestination(cargo.getDestination());
                        this.listOfCargo.add(cargo);
                        this.weight += cargo.getWeight();
                    }
                    if (!this.listOfCargo.get(0).getDestination().equals(cargo.getDestination())) {
                        this.listOfCargo.remove(cargo);
                        closeContainer();
                    }
                    break;
            }
        }
        for (Cargo cargoToRemove : this.listOfCargo) {
            listOfNewCargo.remove(cargoToRemove);
        }
        return listOfNewCargo;
    }

    /**
     * Gets the total weight of cargo in the container
     * @return total weight
     */
    public double calculateWeightContainer() {
        double totalWeight = 0;
        for(Cargo c: this.listOfCargo) {
            totalWeight += c.weight;
        }
        return totalWeight;
    }

    /**
     * Adds a single cargo to container
     * @param newCargo
     */
    public void addCargo(Cargo newCargo) {
        this.listOfCargo.add(newCargo);
        this.weight += newCargo.getWeight();
    }

    /**
     * Sorts cargo based on destination
     * @param destinations
     * @param allCargo
     * @return sorted list of cargo
     */
    public static List<Cargo> sortCargoDest(String[] destinations, List<Cargo> allCargo) {
        List<Cargo> sortedCargoDest = new ArrayList<Cargo>();
        for (int i = 0; i < destinations.length; i++) {
            for (int j = 0; j < allCargo.size(); j++) {
                if (allCargo.get(j).destination.equals(destinations[i])) {
                    sortedCargoDest.add(allCargo.get(j));
                }
            }
        }
        return sortedCargoDest;
    }

    /**
     * Closes the container so no more cargo can be added
     */
    public void closeContainer() {
        this.isOpen = false;
    }

    public double getTotalVolume() {
        return this.length * this.width * this.height;
    }

    /**
     * Gets volume occupied by cargo in container
     * @return total volume
     */
    public double getSpaceFilled() {
        double result = 0;

        for (Cargo c : this.listOfCargo) {
            result += c.getVolume();
        }
        return result;
    }

    public List<Cargo> getListOfCargo() {
        return this.listOfCargo;
    }

    public double getWeight() {
        return this.weight;
    }

    public String getDestination() {
        return this.destination;
    }

    public boolean getIsOpen() {
        return this.isOpen;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Container)) {
            return false;
        }

        Container cont = (Container) obj;

        for (int i = 0; i < listOfCargo.size(); i++) {
            if (!(cont.getListOfCargo().get(i).equals(this.getListOfCargo().get(i)))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "Container [destination=" + destination + ", height=" + height + ", length=" + length + ", listOfCargo="
                + listOfCargo + ", weight=" + weight + ", width=" + width + "]";
    }

}
