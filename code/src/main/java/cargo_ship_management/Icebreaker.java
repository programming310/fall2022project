package cargo_ship_management;

public class Icebreaker extends Boat{

    /**
     * Constructor for Icebreaker
     * @param name
     * @param weightCapacity
     * @param numCompartments
     * @param containersPerComp
     * @param topSpeed
     */
    public Icebreaker(String name, double weightCapacity, int numCompartments, int containersPerComp, double topSpeed) {
        super(name, weightCapacity, numCompartments, containersPerComp, topSpeed);
    }

    /**
     * Copy constructor
     * @param i
     */
    public Icebreaker(Icebreaker i){
        super(i);
    }

    /**
     * Reduces distance on icy routes by breaking the ice
     * @param totalDistance
     * @return distance double
     */
    public double breakIce(double totalDistance) {
        return totalDistance*0.65;
    }
}
