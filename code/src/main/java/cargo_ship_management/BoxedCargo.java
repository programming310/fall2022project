package cargo_ship_management;

public class BoxedCargo extends Cargo{

    private double individualWeight;
    private int numberOfItems;

    /**
     * Constructor for BoxedCargo
     * @param goodsType
     * @param amount
     * @param individualWeight
     * @param individualSpace
     * @param destination
     */
    public BoxedCargo(String goodsType, int amount, double individualWeight, double individualSpace, String destination) {
        super(goodsType, (amount*individualWeight), (amount*individualSpace), destination);
        this.individualWeight = individualWeight;
        this.numberOfItems = amount;
    }

    /**
     * Deep copy constructor for BoxedCargo
     * @param cargo
     */
    public BoxedCargo(BoxedCargo cargo){
        super(cargo);
        this.individualWeight = cargo.getIndividualWeight();
        this.numberOfItems = cargo.getNumberOfItems();
    }

    public double getIndividualWeight(){
        return this.individualWeight;
    }

    public int getNumberOfItems() {
        return this.numberOfItems;
    }
    
    public String toString(){
        return "Cargo [type=" + this.goodsType + ", numberOfItems=" + this.numberOfItems + ", totalWeight=" + this.weight +
         ", totalVolume=" + this.volume + ", destination=" + this.destination + "]";
    }

}
