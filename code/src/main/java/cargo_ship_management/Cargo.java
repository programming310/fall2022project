package cargo_ship_management;

import java.util.ArrayList;
import java.util.List;

public abstract class Cargo {
    String goodsType;
    double weight;
    double volume;
    String destination;

    public Cargo(String goodsType, double weight, double volume, String destination) {
        this.goodsType = goodsType;
        this.weight = weight / 2240; // converting pounds into tons
        this.volume = volume;
        this.destination = destination;
    }

    public Cargo(Cargo cargo) {
        this.goodsType = cargo.getGoodsType();
        this.weight = cargo.getWeight();
        this.volume = cargo.getVolume();
        this.destination = cargo.getDestination();
    }

    public double compareSize(Cargo c) {
        return this.volume - c.getVolume();
    }

    public static List<Cargo> swapPositions(List<Cargo> allCargo, int pos1, int pos2) {
        Cargo temp = allCargo.get(pos1);
        allCargo.set(pos1, allCargo.get(pos2));
        allCargo.set(pos2, temp);

        return allCargo;
    }

    public static List<Cargo> sortDecreasing(List<Cargo> allCargo) {
        int maxIndex;
        for (int i = 0; i < allCargo.size(); i++) {
            maxIndex = i;
            for (int j = i; j < allCargo.size(); j++) {
                if (allCargo.get(maxIndex).compareSize(allCargo.get(j)) < 0)
                    maxIndex = j;
            }
            swapPositions(allCargo, i, maxIndex);
        }

        return allCargo;
    }

    /**
     * Creates deep copy of a list of cargo.
     * 
     * @param allCargo
     * @return List<Cargo>
     */
    public static List<Cargo> duplicateCargoList(List<Cargo> allCargo) {
        List<Cargo> copy = new ArrayList<Cargo>();
        for (Cargo c : allCargo) {
            switch (c.getClass().getSimpleName()) {
                case "MassiveCargo":
                    copy.add(new MassiveCargo((MassiveCargo) c));
                    break;
                case "BoxedCargo":
                    copy.add(new BoxedCargo((BoxedCargo) c));
                    break;
            }
        }
        return copy;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public double getWeight() {
        return this.weight;
    };

    public double getVolume() {
        return this.volume;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cargo)) {
            return false;
        }

        Cargo c = (Cargo) obj;

        return c.getGoodsType().equals(this.goodsType) && c.getWeight() == this.weight && c.getVolume() == this.volume
                && c.getDestination().equals(this.destination);
    }

    @Override
    public String toString() {
        return "Cargo [type=" + this.goodsType + ", weight=" + this.weight +
                ", volume=" + this.volume + ", destination=" + this.destination + "]";
    }
}
