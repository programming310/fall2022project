package cargo_ship_management;

public class Feeders extends Boat{

    /**
     * Constructor for Feeders
     * @param name
     * @param weightCapacity
     * @param numCompartments
     * @param containersPerComp
     * @param topSpeed
     */
    public Feeders(String name, double weightCapacity, int numCompartments, int containersPerComp, double topSpeed) {
        super(name, weightCapacity, numCompartments, containersPerComp, topSpeed);
    }

    /**
     * Copy constructor
     * @param f
     */
    public Feeders(Feeders f){
        super(f);
    }

    /**
     * Reduces distance on a route with canals by taking shortcuts through the canals
     * @param totalDistance
     * @return distance double
     */
    public double passCanal(double totalDistance) {
        return totalDistance*0.70;
    }
}
