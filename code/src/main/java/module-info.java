module cargo_ship_management {
    requires transitive javafx.controls;
    requires transitive javafx.fxml;
    requires transitive java.sql;

    opens cargo_ship_management to javafx.fxml;
    exports cargo_ship_management;
}
