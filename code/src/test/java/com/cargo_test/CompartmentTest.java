package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cargo_ship_management.*;

public class CompartmentTest {

    @Test
    public void testConstructor() {
        Compartment myCompartment = new Compartment(1);
        List<Container> contList = new ArrayList<Container>();
        contList.add(new Container());

        assertNotNull(myCompartment);
        assertEquals(0, myCompartment.getCurrentWeight(), 1);
        assertEquals(contList, myCompartment.getListOfContainers());
    }

    @Test
    public void testToString() {
        List<Container> contList = new ArrayList<Container>();
        contList.add(new Container());
        Compartment myCompartment = new Compartment(1);
        assertEquals(
                "Compartment [currentWeight=" + 0.0 + ", listOfContainers=" + contList.size() + "]",
                myCompartment.toString());
    }

    @Test
    public void testGetCurrentWeight() {
        Compartment c = new Compartment(2);
        Cargo cargo = new BoxedCargo("phone", 5, 50000, 10000, "Sweden");
        c.getListOfContainers().get(0).addCargo(cargo);
        c.updateCurrentWeight();
        assertEquals(111.60714285714286, c.getCurrentWeight(), 0.000001);

        
    }
}
