package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cargo_ship_management.Boat;
import cargo_ship_management.BoxedCargo;
import cargo_ship_management.Cargo;
import cargo_ship_management.Compartment;
import cargo_ship_management.Container;
import cargo_ship_management.Feeders;
import cargo_ship_management.Icebreaker;
import cargo_ship_management.International;
import cargo_ship_management.MassiveCargo;
import cargo_ship_management.Route;

public class BoatTest {

    @Test
    public void testBoatConstructor() {
        Feeders boat = new Feeders("Suezmax", 2000, 6, 500, 20);

        assertNotNull(boat);
        assertEquals(boat.getName(), "Suezmax");
        assertEquals(2000, boat.getWeightCapacity(), 0.00001);
        assertEquals(6, boat.getCompartments().size());
        assertEquals(500, boat.getCompartments().get(0).getListOfContainers().size(), 0.00001);
        assertEquals(20, boat.getTopSpeed(), 0.00001);
    }

    @Test
    public void testToString() {
        Feeders boat = new Feeders("Suezmax", 2000, 6, 3, 20);
        Compartment comp = new Compartment(3);

        String compartmentsDisplay = comp.toString();
        compartmentsDisplay = compartmentsDisplay.substring(1, compartmentsDisplay.length() - 1);

        String expected = "\n Boat Name: Suezmax" +
        "\n Weight Capacity: 2000.0"+
        "\n Speed: 20.0km/h\n"+
       
       "\n compartments: "+
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3, " +
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3, " +
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3, " +
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3, " +
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3, " +
       "\nCompartment:  currentWeight: 0.0 	Number Of Containers: 3"; 
       

        assertEquals(expected, boat.toString());
    }

    @Test
    public void testOptimizeSpace() {

        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));
        allCargo.add(new BoxedCargo("Wheels", 8, 100, 0.65, "Italy"));
        allCargo.add(new BoxedCargo("Chairs", 4, 15, 0.5, "Sweden"));
        allCargo.add(new BoxedCargo("Tables", 2, 35, 1.2, "Sweden"));

        Boat smallBoat = new Feeders("Little boat", 10000, 1, 6, 30);

        smallBoat.optimizeForSpace(allCargo);

        String result = "";
        for (Container c : smallBoat.getCompartments().get(0).getListOfContainers()) {
            result += c.toString();
        }

        assertEquals("Container [destination=null, height=2.28, length=6.0, " +
                "listOfCargo=[Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada]], weight=2.232142857142857, width=2.34]"
                +
                "Container [destination=null, height=2.28, length=6.0, listOfCargo=[Cargo [type=car, weight=1.1160714285714286, "
                +
                "volume=15.0, destination=London]], weight=1.1160714285714286, width=2.34]Container [destination=null, height=2.28, "
                +
                "length=6.0, listOfCargo=[Cargo [type=Wheels, numberOfItems=8, totalWeight=0.35714285714285715, totalVolume=5.2, destination=Italy]], "
                +
                "weight=0.35714285714285715, width=2.34]Container [destination=null, height=2.28, length=6.0, listOfCargo=[Cargo [type=motorcycle, "
                +
                "weight=0.44642857142857145, volume=5.0, destination=Usa]], weight=0.44642857142857145, width=2.34]Container [destination=null, "
                +
                "height=2.28, length=6.0, listOfCargo=[Cargo [type=Tables, numberOfItems=2, totalWeight=0.03125, totalVolume=2.4, destination=Sweden], "
                +
                "Cargo [type=Chairs, numberOfItems=4, totalWeight=0.026785714285714284, totalVolume=2.0, destination=Sweden]], "
                +
                "weight=0.05803571428571429, width=2.34]Container [destination=null, height=2.28, length=6.0, listOfCargo=[], "
                +
                "weight=0.0, width=2.34]", result);

    }

    @Test
    public void testOptimizeDestination() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));
        allCargo.add(new BoxedCargo("Wheels", 8, 100, 0.65, "Italy"));
        allCargo.add(new BoxedCargo("Chairs", 4, 15, 0.5, "Sweden"));
        allCargo.add(new BoxedCargo("Tables", 2, 35, 1.2, "Sweden"));

        String[] destinationsOrder = new String[] { "Iceland", "Sweden", "Norway", "Finland", "Russia", "Usa", "Canada",
                "Greenland",
                "Portugal", "Spain", "France", "Italy", "Grece", "Turkey", "Egypt" };

        Boat testBoat = new Feeders("Test boat", 1000, 5, 3, 30);

        testBoat.optimizeDestination(destinationsOrder, allCargo);

        String result = "";

        for (int i = 0; i < testBoat.getCompartments().size(); i++) {
            for (Container c : testBoat.getCompartments().get(i).getListOfContainers()) {
                result += c.toString();
            }
        }

        String expected = "Container [destination=Sweden, height=2.28, length=6.0, listOfCargo=[Cargo [type=Chairs, numberOfItems=4, totalWeight=0.026785714285714284, totalVolume=2.0, destination=Sweden], Cargo [type=Tables, numberOfItems=2, totalWeight=0.03125, totalVolume=2.4, destination=Sweden]], weight=0.05803571428571429, width=2.34]"
                + "Container [destination=Usa, height=2.28, length=6.0, listOfCargo=[Cargo [type=motorcycle, weight=0.44642857142857145, volume=5.0, destination=Usa]], weight=0.44642857142857145, width=2.34]"
                + "Container [destination=Canada, height=2.28, length=6.0, listOfCargo=[Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada]], weight=2.232142857142857, width=2.34]"
                + "Container [destination=Italy, height=2.28, length=6.0, listOfCargo=[Cargo [type=Wheels, numberOfItems=8, totalWeight=0.35714285714285715, totalVolume=5.2, destination=Italy]], weight=0.35714285714285715, width=2.34]"
                + "Container [destination=null, height=2.28, length=6.0, listOfCargo=[], weight=0.0, width=2.34]Container [destination=null, height=2.28, length=6.0, listOfCargo=[], weight=0.0, width=2.34]";

        assertEquals(expected, result);

    }

    @Test
    public void testGetTotalWeight() {
        Feeders smallBoat = new Feeders("Small boat", 1, 1, 2, 20);
        Cargo cargo = new BoxedCargo("phone", 5, 50000, 10000, "Sweden"); 
        smallBoat.getCompartments().get(0).getListOfContainers().get(0).addCargo(cargo);

        assertEquals(111.60714285714286, smallBoat.getTotalWeight(), 0.00001);
    }

    @Test
    public void testCalculateSpeed() {
        double speed = 0;
        Feeders smallBoat = new Feeders("Small boat", 1, 1, 2, 20);
        Cargo cargo = new BoxedCargo("phone", 5, 50, 100, "Sweden"); 
        smallBoat.getCompartments().get(0).getListOfContainers().get(0).addCargo(cargo);
        
        speed = smallBoat.calculateSpeed();
        assertEquals(18.883928571428573, speed, 0.00001);
    }

    @Test
    public void testCalculateTime() {
        double time = 0;
        Feeders smallBoat = new Feeders("Small boat", 1, 1, 2, 20);
        Cargo cargo = new BoxedCargo("phone", 5, 50, 100, "Sweden"); 
        smallBoat.getCompartments().get(0).getListOfContainers().get(0).addCargo(cargo);
        Route r = new Route(1, new String[]{"Oslo, Sweeden, Canada, USA"}, 3, true, false, true);
        List<Route> listRoute = new ArrayList<Route>();
        listRoute.add(r);
        listRoute.add(r);
        listRoute.add(r);

        time = smallBoat.calculateTime(listRoute);
        assertEquals(0.33361702127659565, time, 0.00001);
    }

    @Test
    public void testDuplicateBoatList(){

        List<Boat> boats = new ArrayList<Boat>();
        Boat b1 = new Icebreaker("Sevmorput", 100, 2, 20, 30.0);
        Boat b2 = new International("Suezmax", 150, 3, 15, 40.0);
        boats.add(b1);
        boats.add(b2);

        List<Boat> deepCopy = Boat.duplicateBoatList(boats);

        assertFalse(boats == deepCopy);
        assertFalse(boats.get(0) == deepCopy.get(0));
        assertFalse(boats.get(1) == deepCopy.get(1));
        assertEquals(boats.get(0).toString(), deepCopy.get(0).toString());
        assertEquals(boats.get(1).toString(), deepCopy.get(1).toString());

    }
}
