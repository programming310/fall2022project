package com.cargo_test;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import cargo_ship_management.*;

public class IceBreakerTest {

    @Test
    public void testConstructor() {
        Icebreaker boat = new Icebreaker("World Traveller", 2000, 6, 500, 20);

        assertNotNull(boat);
        assertEquals(boat.getName(), "World Traveller");
        assertEquals(2000, boat.getWeightCapacity(), 0.00001);
        assertEquals(6, boat.getCompartments().size());
        assertEquals(500, boat.getCompartments().get(0).getListOfContainers().size(), 0.00001);
        assertEquals(20, boat.getTopSpeed(), 0.00001);
    }

}
