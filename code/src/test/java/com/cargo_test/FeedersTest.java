package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import org.junit.Test;

import cargo_ship_management.*;

public class FeedersTest {

    @Test
    public void constructorTest() {
        Feeders boat = new Feeders("Suezmax", 2000, 6, 500, 20);

        assertNotNull(boat);
        assertEquals(boat.getName(), "Suezmax");
        assertEquals(2000, boat.getWeightCapacity(), 0.00001);
        assertEquals(6, boat.getCompartments().size());
        assertEquals(500, boat.getCompartments().get(0).getListOfContainers().size(), 0.00001);
        assertEquals(20, boat.getTopSpeed(), 0.00001);
    }

}
