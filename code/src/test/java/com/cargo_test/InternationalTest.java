package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cargo_ship_management.*;

public class InternationalTest {

    @Test
    public void constructorTest() {
        International boat = new International("50 Let Pobedy", 2000, 6, 500, 20);

        assertNotNull(boat);
        assertEquals(boat.getName(), "50 Let Pobedy");
        assertEquals(2000, boat.getWeightCapacity(), 0.00001);
        assertEquals(6, boat.getCompartments().size());
        assertEquals(500, boat.getCompartments().get(0).getListOfContainers().size(), 0.00001);
        assertEquals(20, boat.getTopSpeed(), 0.00001);
        }
        
    }
        