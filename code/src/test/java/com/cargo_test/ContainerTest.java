package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cargo_ship_management.*;

public class ContainerTest {
    @Test
    public void testConstructor() {

        List<Cargo> listOfCargos = new ArrayList<Cargo>();
        Container myCont = new Container();

        assertNotNull(myCont);
        assertEquals(listOfCargos, myCont.getListOfCargo());
        assertEquals(0.0, myCont.getWeight(), 0.00001);
    }

    @Test
    public void testSortCargoDest() {
        String[] destinations = new String[] { "Iceland", "Sweden", "Norway", "Finland", "Russia", "Usa", "Canada",
                "Greenland",
                "Portugal", "Spain", "France", "Italy", "Grece", "Turkey", "Egypt" };
        Container newCont = new Container();
        newCont.addCargo(new BoxedCargo("phone", 100, 3000000, 56, "Norway"));
        newCont.addCargo(new BoxedCargo("Keyboard", 3000, 450000, 5, "Egypt"));
        newCont.addCargo(new BoxedCargo("mouse", 2300, 30000, 40, "Canada"));

        List<Cargo> newListCargo = Container.sortCargoDest(destinations, newCont.getListOfCargo());
        System.out.println(newListCargo);
        String expected = "[Cargo [type=" + "phone" + ", numberOfItems=" + 100 + ", totalWeight=" + 133928.57142857142 +
                ", totalVolume=" + 5600.0 + ", destination=" + "Norway" + "], " + "Cargo [type=" + "mouse"
                + ", numberOfItems=" + 2300 + ", totalWeight=" + 30803.571428571428 +
                ", totalVolume=" + 92000.0 + ", destination=" + "Canada" + "], " + "Cargo [type=" + "Keyboard"
                + ", numberOfItems=" + 3000 + ", totalWeight=" + 602678.5714285715 +
                ", totalVolume=" + 15000.0 + ", destination=" + "Egypt" + "]]";

        assertEquals(expected, Container.sortCargoDest(destinations, newListCargo).toString());
    }

    @Test
    public void testToString() {
        Container myCont = new Container();

        assertEquals(
                "Container [destination=" + null + ", height=" + 2.28 + ", length=" + 6.0 + ", listOfCargo="
                        + new ArrayList<Cargo>() + ", weight=" + 0.0 + ", width=" + 2.34 + "]",
                myCont.toString());
    }
}
