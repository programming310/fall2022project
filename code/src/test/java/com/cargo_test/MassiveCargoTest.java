package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cargo_ship_management.*;

public class MassiveCargoTest {
    @Test
    public void testConstructor() {
        MassiveCargo myCargo = new MassiveCargo("Super Cars", 90, 50,"Oslo");

        assertNotNull(myCargo);
        assertEquals("Super Cars", myCargo.getGoodsType());
        assertEquals(90, myCargo.getWeight(), 0.00001);
        assertEquals(50, myCargo.getVolume(), 0.00001);
        assertEquals("Oslo", myCargo.getDestination());
    }

}
