package com.cargo_test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cargo_ship_management.Cargo;
import cargo_ship_management.MassiveCargo;

public class CargoTest {

    @Test
    public void testSortDecreasing() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));

        String expected = "[Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada],"
                +
                " Cargo [type=car, weight=1.1160714285714286, volume=15.0, destination=London],"
                +
                " Cargo [type=motorcycle, weight=0.44642857142857145, volume=5.0, destination=Usa]]";

        allCargo = Cargo.sortDecreasing(allCargo);
        assertEquals(expected, allCargo.toString());
    }

    @Test
    public void testDuplicateCargoList() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));

        allCargo = Cargo.duplicateCargoList(allCargo);
        String expected = "[Cargo [type=motorcycle, weight=0.44642857142857145, volume=5.0, destination=Usa],"
                +
                " Cargo [type=car, weight=1.1160714285714286, volume=15.0, destination=London],"
                +
                " Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada]]";

        // System.out.println(allCargo.toString());
        System.out.println(expected);
        assertEquals(expected, allCargo.toString());

    }

    @Test
    public void testSwapPosition() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));

        allCargo = Cargo.swapPositions(allCargo, 0, 1);

        String expected = "[Cargo [type=car, weight=1.1160714285714286, volume=15.0, destination=London],"
                +
                " Cargo [type=motorcycle, weight=0.44642857142857145, volume=5.0, destination=Usa],"
                +
                " Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada]]";
        assertEquals(expected, allCargo.toString());
    }

    @Test
    public void testCompareSize() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));

        double value = allCargo.get(1).compareSize(allCargo.get(0));

        assertEquals(10, value, 0.1);
    }

    @Test
    public void testToString() {
        List<Cargo> allCargo = new ArrayList<Cargo>();
        allCargo.add(new MassiveCargo("motorcycle", 1000, 5, "Usa"));
        allCargo.add(new MassiveCargo("car", 2500, 15, "London"));
        allCargo.add(new MassiveCargo("truck", 5000, 30, "Canada"));

        String expected = "[Cargo [type=motorcycle, weight=0.44642857142857145, volume=5.0, destination=Usa],"
        + 
        " Cargo [type=car, weight=1.1160714285714286, volume=15.0, destination=London],"
        + 
        " Cargo [type=truck, weight=2.232142857142857, volume=30.0, destination=Canada]]";
        assertEquals(expected, allCargo.toString());
    }

}
