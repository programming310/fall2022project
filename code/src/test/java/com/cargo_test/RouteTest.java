package com.cargo_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import cargo_ship_management.Route;

public class RouteTest {

    @Test
    public void testConstructor() {
        Route myRoute = new Route(1, new String[] { "Brazil", "USA", "Canada", "Japan" }, 1500, false, true, true);
        String[] destinations = new String[]{"Brazil", "USA", "Canada", "Japan"};
        assertNotNull(myRoute);
        assertArrayEquals(destinations, myRoute.getDestinations());
        assertEquals(1, myRoute.getRouteId());
        assertEquals(1500, myRoute.getTotalDistance(), 0.00001);
        assertEquals(false, myRoute.isIcy());
        assertEquals(true, myRoute.isOcean());
        assertEquals(true, myRoute.isCanals());
    }

    @Test
    public void testToString() {
        Route myRoute = new Route(1, new String[] { "Brazil", "USA", "Canada", "Japan" }, 1500, false, true, true);
        assertEquals("Route [routeId=" + 1 + ", destinations=" + "[Brazil, USA, Canada, Japan]" + ", totalDistance="
        + 1500.0 + ", isIcy=" + false + ", isOcean=" + true + ", isCanals=" + true + "]", myRoute.toString());
    }
}
