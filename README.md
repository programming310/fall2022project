# fall2022Project

Cargo Ship Management Program

## Description
This project is designed to optimize the configuration of cargo on a cargo ship given  a list of cargo, a list of ships and a list of routes. There are three ways that
the user can choose to optimize: to minimize space, for fastest delivery and for easy unpacking of the cargo. The user will be able to import data either from csv files
or through the pdbora19c database. The program can be run both in the terminal and in a javafx application.


## Installation
To setup the program, start by pulling the files into your system from the gitlab repository. When opening the project in an editor, make sure that the source folder is fall2022project.
To setup the database, connect to pdbora19c and run the dbSetup.sql script. This will create the necessary tables and insert sample data for ships and routes. If you wish to also insert
sample data for cargo into the database, you can run the insertSampleCargo.sql script. 

If you instead want to generate a new randomized set of cargo, you can run the java file at code\src\main\java\cargo_ship_management\DataGenerator.java. Keep in mind however that
this will only append new cargo to both cargoList.csv and the cargoList table in the database, not overwrite the existing data. 

Once you have all your data, you can run the program at code\src\main\java\cargo_ship_management\CargoShipManagement.java. When running, you will have the option to run the program
in either the terminal or the javafx application and the choice of either importing from the csv files or the database. 

## UML Diagram
![Uml diagram image](./UML/UMLdiagram.jpg)
    


Created by
Michael Derocher and Samir-Abo Asfour 

