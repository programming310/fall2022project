DROP TABLE ships;
DROP TABLE routeList;
DROP TABLE cargoList;


CREATE TABLE routeList (
    routeNum varchar2(2) PRIMARY KEY,
    destinations varchar2(300),
    totalDistance number(5,0),
    icy char(1),
    ocean char(1),
    canals char(1)
);
/
CREATE TABLE ships (
    type varchar2(20),
    name varchar2(20),
    weightCap number(6,0),
    numComps number(2,0),
    spacePerComp number(5,0),
    topSpeed number(4,2)
);
/
CREATE TABLE cargoList (
    type varchar2(20),
    itemName varchar2(40),
    amount number(5,0),
    individualWeight number(7,2),
    individualVolume number(7,5),
    destination varchar2(15)
);
/
INSERT INTO routeList VALUES('1', 'Iceland:Sweden:Norway:Finland:Russia:Usa:Canada:Greenland',
    345, 'x', null, null);
INSERT INTO routeList VALUES('2', 'Portugal:Spain:France:Italy:Grece:Turkey:Egypt',
    4235, null, null, 'x');
    
INSERT INTO ships VALUES('Icebreaker', 'Sevmorput', 60, 6, 20, 38.5);
INSERT INTO ships VALUES('International', 'Suezmax', 100, 5, 24, 22.2);

/
Select * from cargoList;
/
Select * from ships;
/
Select * from routeList;